# JS-BEM

Css in JS with BEM standard.

### Unit test
[![build status](https://gitlab.com/nicoandresr/js-bem/badges/master/build.svg)](https://gitlab.com/nicoandresr/js-bem/commits/master)

## install

    npm install js-bem

## Synopsis

Covert javascript objects to css in js with bem standard and, generate BEM blocks, elements and modifiers.

```javascript
var test = require('tape');

// you must import like
// import BEM, { Modifier } from 'js-bem';
var bemJs = require('./umd');
var BEM = bemJs.default;
var Modifier = bemJs.Modifier;
// debugMode is an optional parameter
// you could omit it.
var debugMode = true;

var css = new BEM({
  heading: {
    color: 'blue',

    title: {
      color: 'green',

      secondary: new Modifier({
        fontSize: '.5rem'
      })
    }
  },
  main: {
    fontSize: '1rem'
  },
  changeCase: {
    color: 'blue',

    elementCase: {
      color: 'yellow'
    },

    modifierCase: new Modifier({
      color: 'silver'
    })
  }
}, debugMode);

test('Generate BEM blocks', function(t) {
  t.plan(2);
  t.equal(css.heading.bem(), 'heading0');
  t.equal(css.main.bem(), 'main3');
});
```
Generate BEM elements
```javascript
test('Generate BEM elements', function(t) {
  t.plan(1);
  t.equal(css.heading.title.bem(), 'heading0__title1');
});
```
Generate BEM modifiers
```javascript
test('Generate BEM modifiers', function(t) {
  t.plan(1);
  const expected = 'heading0__title1 heading0__title1--secondary2';
  t.equal(css.heading.title.secondary.bem(), expected);
});
```
Generate BEM modifiers by props
```javascript
test('Generate BEM modifiers by props', function(t) {
  t.plan(1);
  const actual = css.heading.title.mod({ secondary: true });
  const expected = 'heading0__title1 heading0__title1--secondary2';
  t.equal(actual, expected);
});
```
Change camel to kebab case for blocks names
```javascript
test('Change camel to kebab case for blocks', function(t) {
  t.plan(1);
  const actual = css.changeCase.bem();
  const expected = 'change-case4';
  t.equal(actual, expected);
});
```
Change camel to kebab case for elements names
```javascript
test('Change camel to kebab case for elements', function(t) {
  t.plan(1);
  const actual = css.changeCase.elementCase.bem();
  const expected = 'change-case4__element-case5';
  t.equal(actual, expected);
});
```
Change camel to kebab case for modifiers names
```javascript
test('Change camel to kebab case for modifiers', function(t) {
  t.plan(1);
  const actual = css.changeCase.modifierCase.bem();
  const expected = 'change-case4--modifier-case6';
  t.equal(actual, expected);
});
```
