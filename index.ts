import camelToKebab from 'js-camel-to-kebab';

import Modifier from './modifier.class';

let counter = 0;

class Bem {
  _classes;
  _blocksModifiers;
  _elementsModifiers;
  _stringBlock;
  _stringElement;
  _stringModifier;
  _styles;

  constructor(styles, debug = false) {
    this.initialize(styles);
    this.generateBlocks();
    if(!debug) this.appendStylesToHeader();
    return this._classes;
  }

  initialize = styles => {
    this._classes = {};
    this._blocksModifiers = {};
    this._elementsModifiers = {};
    this._stringBlock = '';
    this._stringElement = '';
    this._stringModifier = '';
    this._styles = styles;
  }

  getModifier = (items, defaultName, modifiers) => {
    let result = defaultName;
    for(const item in items) {
      if(items[item] === true && modifiers[item]) {
        result += ` ${modifiers[item]}`;
      }
    }

    return result;
  }

  appendStylesToHeader = () => {
    document.head.appendChild(
      document.createElement('style')
    ).textContent = this._stringBlock;
  }

  generateProperties = (styles) => {
    for(const prop in styles) {
      const value = styles[prop];
      this._stringModifier += ` ${prop}: ${value};`;
    }
  }

  generateBlocks = () => {
    for(const block in this._styles) {
      const blockName = `${camelToKebab(block)}${counter++}`;
      this._classes[block] = {};
      this._classes[block].bem = () => blockName;
      this._classes[block].mod = items => this.getModifier(items, blockName, this._blocksModifiers);
      this._stringBlock += `.${blockName} {`;
      this.generateElements(block, blockName);
      this._stringBlock += ' } ';
      this._stringBlock += this._stringElement;
      this._stringBlock += this._stringModifier;
    }
  }

  generateElements = (block, blockName) => {
    for (const element in this._styles[block]) {
      if (typeof(this._styles[block][element]) === 'string') {
        const value = this._styles[block][element];
        this._stringBlock += ` ${camelToKebab(element)}: ${value};`;
      } else { 
        const separator = this._styles[block][element] instanceof Modifier ? '--' : '__';
        const elementName = `${blockName}${separator}${camelToKebab(element)}${counter++}`;
        this._classes[block][element] = {};
        this._classes[block][element].bem = () => elementName;
        this._classes[block][element].mod = items => this.getModifier(items, elementName, this._elementsModifiers);
        this._stringElement += `.${elementName} {`;
        this.generateModifiers(block, element, elementName);
        this._stringElement += ' } ';
        if (this._styles[block][element] instanceof Modifier) {
          this._blocksModifiers[element] = elementName;
        }
      }
    }
  }

  generateModifiers = (block, element, elementName) => {
    for (const modifier in this._styles[block][element]) {
      if (typeof(this._styles[block][element][modifier]) === 'string') {
        const value = this._styles[block][element][modifier];
        this._stringElement += ` ${camelToKebab(modifier)}: ${value};`;
      } else {
        const modifierName = `${elementName}--${camelToKebab(modifier)}${counter++}`;
        this._classes[block][element][modifier] = {};
        this._classes[block][element][modifier].bem = () => `${elementName} ${modifierName}`;
        this._stringModifier += `.${modifierName} {`;
        this.generateProperties(this._styles[block][element][modifier]);
        this._stringModifier += ' } ';
        this._elementsModifiers[modifier] = modifierName;
      }
    }
  }
};

export { default as Modifier } from './modifier.class';
export default Bem;
