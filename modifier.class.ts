class Modifier {
  constructor(values) {
    Object.assign(this, values);
  }
}

export default Modifier;
